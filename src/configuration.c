/*
 * Copyright 2019 Robert 'Bobby' Zenz
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef CONFIGURATION_C
#define CONFIGURATION_C

#include <getopt.h>
#include <limits.h>
#include <stdbool.h>


struct configuration {
	bool error;
	bool help;
	int lowerBound;
	int upperBound;
	char* string;
};


static struct option options[] = {
	{ "help", no_argument, 0, 'h' },
	{ "lower-bound", required_argument, 0, 'l' },
	{ "upper-bound", required_argument, 0, 'u' },
	{ NULL, 0, NULL, 0 }
};


struct configuration configure(int argcount, char** args) {
	struct configuration config;
	config.error = false;
	config.help = false;
	config.lowerBound = INT_MIN;
	config.upperBound = INT_MAX;
	config.string = NULL;
	
	int value = 0;
	int optionindex = 0;
	
	while((value = getopt_long(
					   argcount,
					   args,
					   "l:u:",
					   options,
					   &optionindex)) != -1) {
		if (value == 0) {
			struct option option = options[optionindex];
			
			if (strcmp(option.name, "help") == 0) {
				config.help = true;
			} else if (strcmp(option.name, "lower-bound") == 0) {
				config.lowerBound = atoi(optarg);
			} else if (strcmp(option.name, "upper-bound") == 0) {
				config.upperBound = atoi(optarg);
			}
		} else if (value == 'l') {
			config.lowerBound = atoi(optarg);
		} else if (value == 'u') {
			config.upperBound = atoi(optarg);
		} else if (value == '?') {
			config.error = true;
		}
	}
	
	if (args[optind] != NULL) {
		config.string = args[optind];
	} else {
		config.error = true;
	}
	
	return config;
}

#endif

