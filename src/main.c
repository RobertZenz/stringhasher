/*
 * Copyright 2019 Robert 'Bobby' Zenz
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <limits.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "configuration.c"
#include "murmurhash1.c"

int main(int argc, char** argv) {
	struct configuration config = configure(argc, argv);
	
	if (config.error) {
		fprintf(stderr, "Try 'stringhasher --help' for more information.\n");
		
		return EXIT_FAILURE;
	}
	
	if (config.help) {
		printf(
			"USAGE: stringhasher [OPTION] STRING\n"
			"\n"
			"  -l, --lower-bound=VALUE  The lower bound (inclusive)."
			"  -u, --upper-bound=VALUE  The upper bound (inclusive).");
			
		return EXIT_SUCCESS;
	}
	
	int hash = MurmurHash(config.string, strlen(config.string), 1);
	
	// Transform the hash to the target range.
	double originalRange = (double)((long)INT_MAX - (long)INT_MIN);
	double requestedRange = (double)((long)config.upperBound - (long)config.lowerBound);
	
	hash = (int)round((((double)hash - (long)INT_MIN) / originalRange) * requestedRange + (double)config.lowerBound);
	
	printf("%i", hash);
}
